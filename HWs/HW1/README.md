﻿## HW1
### Physical Layer Questions

 This Homework contains questions about:
 - Transmission Between Nodes and Hops in Networks.
 - Methods for estimating propagation delay and transmition delay in transmitting packets between source and destination.
 - Using CDMA system for transmitting symbols.
 - Using NRZ, MLT-3, and RZ encoding
 - Computing Frequency Allocated for Different Cells
 - Transmission Loss and Power
 - Ultra-Reliable Low-Latency Communication (URLLC) service


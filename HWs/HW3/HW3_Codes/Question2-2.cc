
#include "ns3/core-module.h"
#include "ns3/point-to-point-module.h"
#include "ns3/network-module.h"
#include "ns3/applications-module.h"
#include "ns3/mobility-module.h"
#include "ns3/csma-module.h"
#include "ns3/internet-module.h"
#include "ns3/yans-wifi-helper.h"
#include "ns3/ssid.h"
#include "ns3/config.h"
#include "ns3/flow-monitor-helper.h"
#include "ns3/ipv4-flow-classifier.h"
#include "ns3/netanim-module.h"

using namespace ns3;
NS_LOG_COMPONENT_DEFINE ("Question 2-2");
int 
main (int argc, char *argv[])
{



	
  UintegerValue minCw = 255;
  
  int packetSize = 1400;
  
  Config::SetDefault ("ns3::Txop::MinCw", UintegerValue (minCw));
  
  NodeContainer wifiStaNode;
  wifiStaNode.Create (n);   // Create n station node objects
  NodeContainer wifiApNode;
  wifiApNode.Create (1);   // Create 1 access point node object
  
  // Create a channel helper and phy helper, and then create the channel
  YansWifiChannelHelper channel = YansWifiChannelHelper::Default ();
  YansWifiPhyHelper phy = YansWifiPhyHelper::Default ();
  phy.SetChannel (channel.Create ());
  
  // Create a WifiMacHelper, which is reused across STA and AP configurations
  WifiMacHelper mac;
  
  // Create a WifiHelper, which will use the above helpers to create
  // and install Wifi devices.  Configure a Wifi standard to use, which
  // will align various parameters in the Phy and Mac to standard defaults.
  WifiHelper wifi;
  wifi.SetStandard (WIFI_PHY_STANDARD_80211g);
  
  // Declare NetDeviceContainers to hold the container returned by the helper
  NetDeviceContainer wifiStaDevices;
  NetDeviceContainer wifiApDevice;
  
  wifi.SetRemoteStationManager ("ns3::IdealWifiManager");
  // Perform the installation
  mac.SetType ("ns3::StaWifiMac");
  wifiStaDevices = wifi.Install (phy, mac, wifiStaNode);
  mac.SetType ("ns3::ApWifiMac");
  wifiApDevice = wifi.Install (phy, mac, wifiApNode);
  
  
  MobilityHelper mobility;

  mobility.SetPositionAllocator ("ns3::GridPositionAllocator",
                                 "MinX", DoubleValue (0.0),
                                 "MinY", DoubleValue (0.0),
                                 "DeltaX", DoubleValue (5.0),
                                 "DeltaY", DoubleValue (10.0),
                                 "GridWidth", UintegerValue (3),
                                 "LayoutType", StringValue ("RowFirst"));

 
  

  mobility.SetMobilityModel ("ns3::ConstantPositionMobilityModel");
  
  mobility.Install (wifiStaNode);
  mobility.Install (wifiApNode);

  InternetStackHelper stack;
  stack.Install (wifiApNode);
  stack.Install (wifiStaNode);

  Ipv4AddressHelper address;

  address.SetBase ("192.168.095.0", "255.255.255.0");
  Ipv4InterfaceContainer staNodeInterfaces, apNodeInterface;
 
  staNodeInterfaces = address.Assign (wifiStaDevices);
  apNodeInterface = address.Assign (wifiApDevice);
 
  // 7. Install applications: two CBR streams each saturating the channel
  ApplicationContainer cbrApps;
  uint16_t cbrPort = 12345;
  OnOffHelper onOffHelper ("ns3::UdpSocketFactory", Address (InetSocketAddress (apNodeInterface.GetAddress (0), cbrPort)));
  onOffHelper.SetAttribute ("PacketSize", UintegerValue (packetSize));
  onOffHelper.SetAttribute ("OnTime",  StringValue ("ns3::ConstantRandomVariable[Constant=1]"));
  onOffHelper.SetAttribute ("OffTime", StringValue ("ns3::ConstantRandomVariable[Constant=0]"));
  
  //Flow 1
  onOffHelper.SetAttribute ("DataRate", StringValue ("40000000bps"));
  onOffHelper.SetAttribute ("StartTime", TimeValue (Seconds (1.000000)));
  cbrApps.Add (onOffHelper.Install (wifiStaNode.Get (0)));
  


  // 8. Install FlowMonitor on all nodes
  FlowMonitorHelper flowmon;
  Ptr<FlowMonitor> monitor = flowmon.InstallAll ();
  
  
  cbrApps.Start (Seconds (1.0));
  cbrApps.Stop (Seconds (2.0));
 
  Ipv4GlobalRoutingHelper::PopulateRoutingTables ();
  
  // Pcap File
  AsciiTraceHelper ascii;
  phy.EnableAscii(ascii.CreateFileStream ("Question2.tr"), wifiApDevice);
  phy.EnablePcap("Question2", wifiApDevice, false);
  
  // Net anim
  AnimationInterface anim ("animation.xml");  // where "animation.xml" is any arbitrary filename
  /*
  anim.SetConstantPosition (wifiStaNode.Get (0), 0.0, 10.0);
  anim.SetConstantPosition (wifiStaNode.Get (1), 5.0, 10.0);
  anim.SetConstantPosition (wifiStaNode.Get (2), 10.0, 10.0);
  anim.SetConstantPosition (wifiStaNode.Get (3), 15.0, 10.0);
  anim.SetConstantPosition (wifiStaNode.Get (4), 20.0, 10.0);
  anim.SetConstantPosition (wifiStaNode.Get (5), 25.0, 10.0);
  anim.SetConstantPosition (wifiStaNode.Get (6), 30.0, 10.0);
  anim.SetConstantPosition (wifiStaNode.Get (7), 35.0, 10.0);
  anim.SetConstantPosition (wifiStaNode.Get (8), 40.0, 10.0);
  anim.SetConstantPosition (wifiStaNode.Get (9), 45.0, 10.0);
  anim.SetConstantPosition (wifiApNode.Get (0), 50.0, 10.0);
  */

  // 9. Run simulation for 10 seconds
  Simulator::Stop (Seconds (4));
  Simulator::Run ();
 


  // 10. Print per flow statistics
  // 10. Print per flow statistics
  monitor->CheckForLostPackets ();
  Ptr<Ipv4FlowClassifier> classifier = DynamicCast<Ipv4FlowClassifier> (flowmon.GetClassifier ());
  FlowMonitor::FlowStatsContainer stats = monitor->GetFlowStats ();
  for (std::map<FlowId, FlowMonitor::FlowStats>::const_iterator i = stats.begin (); i != stats.end (); ++i)
    {	
      
      
      if (i->first > 0)
        {
          
          Ipv4FlowClassifier::FiveTuple t = classifier->FindFlow (i->first);
          
          std::cout << "Flow " << i->first << " (" << t.sourceAddress << " -> " << t.destinationAddress << ")\n";
          std::cout << "  Tx Packets: " << i->second.txPackets << "\n";
          std::cout << "  Tx Bytes:   " << i->second.txBytes << "\n";
          std::cout << "  TxOffered:  " << i->second.txBytes * 8.0 / 9.0 / 1000 / 1000  << " Mbps\n";
          std::cout << "  Rx Packets: " << i->second.rxPackets << "\n";
          std::cout << "  Rx Bytes:   " << i->second.rxBytes << "\n";
          std::cout << "  Throughput: " << i->second.rxBytes * 8.0 / 9.0 / 1000 / 1000  << " Mbps\n";
          
          
          
        }
    }
   Simulator::Destroy ();

 
  return 0;


}

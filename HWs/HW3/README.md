﻿## HW3
### MAC Sublayer Questions

This homework is about the MAC sublayer and its simulation using the NS3 software. NS3 is a powerful  
simulation tool used for testing and analyzing various network protocols and scenarios. It is based on  
the C++ language and provides the user with a predefined API to design a hypothetical network and  
modify the important parameters. The purpose of this exercise is to investigate the effect of rate, packet size and number of users on throughput in MAC sublayer.

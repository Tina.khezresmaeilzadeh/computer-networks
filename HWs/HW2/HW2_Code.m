%% 1.1
% part 1
burst_min_length = 10;            

packet_length = 64;  
Data_Length = 64*1000;   
[num_errors ,num_bits] = deal(0);
    
while num_bits < Data_Length
    % Generate binary data and convert to symbols
    packetIn = randi([0 1],Data_Length,1);
       
        
    y = Channel_With_Burst_Error(packetIn, burst_min_length);
        
        
    y = int32(y);
        
    % Calculate the number of bit errors in the packet.
        
    error_numbers_in_packet = biterr(packetIn,y);
        
        
    % Increment the error and bit counters
    num_errors = num_errors + error_numbers_in_packet;
    num_bits = num_bits + Data_Length;

end
    
% Estimate the BER for both methods
BER = num_errors/num_bits;
%% 1.1
% part 2

burst_min_length = 10;            
Data_Length = 64*1000;
packet_length = 64;    
input_length = int32(Data_Length/3);
[num_errors ,num_bits] = deal(0);
    
while num_bits < Data_Length
    % Generate binary data and convert to symbols
    packetIn = randi([0 1],input_length,1);
        
    % encode the data
    dataEnc = Encoder(packetIn, 1/3);
    
    y = Channel_With_Burst_Error(dataEnc, burst_min_length);
        
        
    %decode the demodulated data
    y_decoded = Decoder(y, 1/3);
        
    % Calculate the number of bit errors in the packet.
        
    error_numbers_in_packet = biterr(packetIn,y_decoded);
    
        
        
    % Increment the error and bit counters
    num_errors = num_errors + error_numbers_in_packet;
    num_bits = num_bits + input_length;

end

% Estimate the BER for both methods
BER = double(num_errors)/double(num_bits);

%% 1.2
% part1

M = 16;             % Modulation order
Data_Length = 64*1000;            
SNR = (1:10)';      % SNR (dB)
packet_length = 64;  
burst_min_length = 10;
BER = zeros(size(SNR)); 

input_length = 21312;

for n = 1:length(SNR)
    
    snrdB = SNR(n);
    % Noise variance calculation for unity average signal power.
    noiseVar = 10.^(-snrdB/10);
    % Reset the error and bit counters
    [num_errors ,num_bits] = deal(0);
    
    while num_bits < Data_Length
        
        
        % Generate binary data and convert to symbols
        packetIn = randi([0 1],input_length,1);
        
        % encode the data
        dataEnc = Encoder(packetIn, 1/3);
        
        % QAM modulate
        modulated_signal = qammod(dataEnc,M,'InputType','bit','UnitAveragePower',true);
        
        % Pass through AWGN channel
        y = Channel_With_Burst_Error(modulated_signal, burst_min_length);
        noisy_signal = awgn(y,snrdB,'measured');
        
        % Demodulate the noisy signal using hard decision (bit) and
        % soft decision (approximate LLR) approaches.
        demodulated_signal = qamdemod(noisy_signal,M,'OutputType','bit','UnitAveragePower',true);
        
        
        %decode the demodulated data
        y_decoded = Decoder(demodulated_signal, 1/3);
        
        % Calculate the number of bit errors in the packet.
        
        error_numbers_in_packet = biterr(packetIn,y_decoded);
        
        
        % Increment the error and bit counters
        num_errors = num_errors + error_numbers_in_packet;
        num_bits = num_bits + input_length;

    end
    
    % Estimate the BER for both methods
    BER(n) = num_errors/num_bits;
    
end

semilogy(SNR,BER,'-*')
grid
xlabel('SNR (dB)')
ylabel('Bit Error Rate')
title('BER-SNR Graph');

%% 1.2
% part 2
M = 16;             % Modulation order
Data_Length = 64*1000;            
SNR = (1:10)';      % SNR (dB)
packet_length = 64;  
burst_min_length = 10;
BER = zeros(size(SNR)); 
  

input_length = 21312;
ordered_array = randperm(input_length);
for n = 1:length(SNR)
    
    snrdB = SNR(n);
    % Noise variance calculation for unity average signal power.
    noiseVar = 10.^(-snrdB/10);
    % Reset the error and bit counters
    [num_errors ,num_bits] = deal(0);
    
    while num_bits < Data_Length
        % Generate binary data and convert to symbols
        packetIn = randi([0 1],input_length,1);
        
        % encode the data
        dataEnc = Encoder(packetIn, 1/3);
        data_interleaved = interleaver(dataEnc, ordered_array);
        % QAM modulate
        modulated_signal = qammod(data_interleaved,M,'InputType','bit','UnitAveragePower',true);
        
        % Pass through AWGN channel
        y = Channel_With_Burst_Error(modulated_signal, burst_min_length);
        noisy_signal = awgn(y,snrdB,'measured');
        
        % Demodulate the noisy signal using hard decision (bit) and
        % soft decision (approximate LLR) approaches.
        demodulated_signal = qamdemod(noisy_signal,M,'OutputType','bit','UnitAveragePower',true);
        y_deinterleaved = deinterleaver(demodulated_signal, ordered_array);
        
        %decode the demodulated data
        y_decoded = Decoder(y_deinterleaved, 1/3);
        
        % Calculate the number of bit errors in the packet.
        
        error_numbers_in_packet = biterr(packetIn,y_decoded);
        
        
        % Increment the error and bit counters
        num_errors = num_errors + error_numbers_in_packet;
        num_bits = num_bits + input_length;

    end
    
    % Estimate the BER for both methods
    BER(n) = num_errors/num_bits;
    
end

figure
semilogy(SNR,BER,'-*')
grid
xlabel('SNR (dB)')
ylabel('Bit Error Rate')
title('BER-SNR with Interleaving');

%% 2.1
% part 2
M = 16; % Modulation order
num_frames = 10;
num_subframes_per_frame = 10;
data_rate = 1.344 *10^6;
frame_time = 10*10^(-3);
subframe_time = 1*10^(-3);            
SNR = (16:25)';      % SNR (dB) 
BER = zeros(size(SNR)); 
Retransmitted_Subframes = zeros(size(SNR));
Data_Length = num_frames*num_subframes_per_frame*subframe_time*data_rate;
subframe_length = subframe_time*data_rate;
frame_length = frame_time*data_rate;
for n = 1:length(SNR)
    
    snrdB = SNR(n);
    % Noise variance calculation for unity average signal power.
    noiseVar = 10.^(-snrdB/10);
    % Reset the error and bit counters
    [num_errors ,num_bits, num_retransmitted_subframes] = deal(0);
    
    while num_bits < Data_Length
        % Generate binary data and convert to symbols
        packetIn = randi([0 1],frame_length,1);
        data= CbCRCGenerator(packetIn, num_subframes_per_frame);
        
        % QAM modulate
        modulated_signal = qammod(data,M,'InputType','bit','UnitAveragePower',true);
        
        % Pass through AWGN channel
        noisy_signal = awgn(modulated_signal,snrdB,'measured');
        
        % Demodulate the noisy signal using hard decision (bit) and
        % soft decision (approximate LLR) approaches.
        demodulated_signal = qamdemod(noisy_signal,M,'OutputType','bit','UnitAveragePower',true);
        
       [y, err] = CbCRCDetector(demodulated_signal, num_subframes_per_frame);
       
        
        % Calculate the number of bit errors in the packet.
        
        error_numbers_in_packet = biterr(packetIn,y);
        
        
        % Increment the error and bit counters
        num_errors = num_errors + error_numbers_in_packet;
        num_bits = num_bits + frame_length;
        num_retransmitted_subframes = num_retransmitted_subframes + sum(err);

    end
    
    % Estimate the BER for both methods
    BER(n) = num_errors/num_bits;
    Retransmitted_Subframes(n) = num_retransmitted_subframes;
end

semilogy(SNR,Retransmitted_Subframes,'-*')
grid
xlabel('SNR (dB)')
ylabel('Retransmitted Subframes')
title('Number of Retransmitted Frames');
%% 2.2 
% part 1
clear
clc
SNR = (1:10).';
Errors = zeros(size(SNR));
for index = 1: length(SNR)
    numOfErrors = 0;
    modOrder = 16;
    bps = log2(modOrder); % Bits per symbol
    EbNo = 2.8; % Energy per bit to noise power spectral density ratio in dB
    EsNo = EbNo + 10*log10(bps); % Energy per symbol to noise power spectral density ratio in dB
    rng default

    turboEnc = comm.TurboEncoder('InterleaverIndicesSource','Input port');
    turboDec = comm.TurboDecoder('InterleaverIndicesSource','Input port','NumIterations',4);
    trellis = poly2trellis(4,[13 15 17],13);
    n = log2(turboEnc.TrellisStructure.numOutputSymbols);
    numTails = log2(turboEnc.TrellisStructure.numStates)*n;
    
    
    errRate = comm.ErrorRate;
    numFrames = 100;
    for pktIdx = 1:numFrames
        L = 1344; % Packet length in bits
        data = randi([0 1],L,1);

        M = L*(2*n - 1) + 2*numTails; % Output codeword packet length
        rate = L/M; % Coding rate for current packet
        %snrdB = EsNo + 10*log10(rate); % Signal to noise ratio in dB
        snrdB = SNR(index);
        noiseVar = 1./(10.^(snrdB/10)); % Noise variance
        [f1, f2] = getf1f2(L);
        Idx = (0:L-1).';
        intrlvrIndices = mod(f1*Idx + f2*Idx.^2, L) + 1;
        %intrlvrIndices = randperm(L);
        encodedData = turboEnc(data,intrlvrIndices);
        modSignal = qammod(encodedData,modOrder,'InputType','bit','UnitAveragePower',true);
        receivedSignal = awgn(modSignal,snrdB);
        demodSignal = qamdemod(receivedSignal,modOrder,'OutputType','llr', ...
            'UnitAveragePower',true,'NoiseVariance',noiseVar);
        %demodSignal = qamdemod(receivedSignal,modOrder,'OutputType','bit','UnitAveragePower',true);
        receivedBits = turboDec(-demodSignal,intrlvrIndices); % Demodulated signal is negated

        errorStats = errRate(data,receivedBits);
        
        
        

        
    end
    numOfErrors = numOfErrors + errorStats(2);
    Errors(index) = numOfErrors/134400;
    
    
end
semilogy(SNR, Errors);
grid
xlabel('SNR (dB)')
ylabel('Bit Erorr Rate')
title('SNR-BER with Turbo Encoder');

%% 2.2 
% part 2
clear
clc
SNR = (1:10).';
Errors = zeros(size(SNR));
for index = 1: length(SNR)
    numOfErrors = 0;
    modOrder = 16;
    bps = log2(modOrder); % Bits per symbol
    EbNo = 2.8; % Energy per bit to noise power spectral density ratio in dB
    EsNo = EbNo + 10*log10(bps); % Energy per symbol to noise power spectral density ratio in dB
    rng default
    
    errRate = comm.ErrorRate;
    numFrames = 100;
    for pktIdx = 1:numFrames
        L = 1344; % Packet length in bits
        data = randi([0 1],L,1);
        snrdB = SNR(index);
        noiseVar = 1./(10.^(snrdB/10)); % Noise variance
        encodedData = Encoder(data, 1/3);
        modSignal = qammod(encodedData,modOrder,'InputType','bit','UnitAveragePower',true);
        receivedSignal = awgn(modSignal,snrdB);
        demodSignal = qamdemod(receivedSignal,modOrder,'OutputType','bit','UnitAveragePower',true);
        %demodSignal = qamdemod(receivedSignal,modOrder,'OutputType','llr', ...
            %'UnitAveragePower',true,'NoiseVariance',noiseVar);
        
        
        receivedBits = Decoder(demodSignal, 1/3); % Demodulated signal is negated

        errorStats = errRate(data,receivedBits);
        
        
        %fprintf('Bit error rate = %5.2e\nNumber of errors = %d\nTotal bits = %d\n',errorStats)

        
    end
    numOfErrors = numOfErrors + errorStats(2);
    Errors(index) = numOfErrors/134400;
    
    
end
semilogy(SNR, Errors);
grid
xlabel('SNR (dB)')
ylabel('Bit Erorr Rate')
title(' SNR-BER With 1/3 Repetative Code');
%% 2.3 
% part 1 and 2
clear
clc
SNR = (5.5:0.5:10).';
Retransmitted_Subframes = zeros(size(SNR));
Errors = zeros(size(SNR));
num_subframes_per_frame = 1;
for index = 1: length(SNR)
    num_retransmitted_subframes = 0;
    numOfErrors = 0;
    modOrder = 16;
    bps = log2(modOrder); % Bits per symbol
    EbNo = 2.8; % Energy per bit to noise power spectral density ratio in dB
    EsNo = EbNo + 10*log10(bps); % Energy per symbol to noise power spectral density ratio in dB
    rng default

    turboEnc = comm.TurboEncoder('InterleaverIndicesSource','Input port');
    turboDec = comm.TurboDecoder('InterleaverIndicesSource','Input port','NumIterations',4);
    trellis = poly2trellis(4,[13 15 17],13);
    n = log2(turboEnc.TrellisStructure.numOutputSymbols);
    numTails = log2(turboEnc.TrellisStructure.numStates)*n;
    
    
    errRate = comm.ErrorRate;
    numFrames = 100;
    for pktIdx = 1:numFrames
        L = 1344 - 16; % Packet length in bits
        data = randi([0 1],L,1);
        data_with_CRC= CbCRCGenerator(data, num_subframes_per_frame);
        LengthWithCRC = length(data_with_CRC);
        M = LengthWithCRC*(2*n - 1) + 2*numTails; % Output codeword packet length
        rate = LengthWithCRC/M; % Coding rate for current packet
        %snrdB = EsNo + 10*log10(rate); % Signal to noise ratio in dB
        snrdB = SNR(index);
        noiseVar = 1./(10.^(snrdB/10)); % Noise variance 
        [f1, f2] = getf1f2(LengthWithCRC);
        Idx = (0:LengthWithCRC-1).';
        intrlvrIndices = mod(f1*Idx + f2*Idx.^2, LengthWithCRC) + 1;
        %intrlvrIndices = randperm(L);
        encodedData = turboEnc(data_with_CRC,intrlvrIndices);
        modSignal = qammod(encodedData,modOrder,'InputType','bit','UnitAveragePower',true);
        receivedSignal = awgn(modSignal,snrdB);
        %demodSignal = qamdemod(receivedSignal,modOrder,'OutputType','llr', ...
            %'UnitAveragePower',true,'NoiseVariance',noiseVar);
        demodSignal = qamdemod(receivedSignal,modOrder,'OutputType','bit','UnitAveragePower',true);
        receivedBits = turboDec(-demodSignal,intrlvrIndices); % Demodulated signal is negated
        [y, err] = CbCRCDetector(receivedBits, num_subframes_per_frame);
        errorStats = errRate(data,y);
        num_retransmitted_subframes = num_retransmitted_subframes + sum(err);
        
        %fprintf('Bit error rate = %5.2e\nNumber of errors = %d\nTotal bits = %d\n',errorStats)

        
    end
    numOfErrors = numOfErrors + errorStats(2);
    Errors(index) = numOfErrors/134400;
    Retransmitted_Subframes(index) = num_retransmitted_subframes;
    
end
figure
semilogy(SNR, Errors);
grid
xlabel('SNR (dB)')
ylabel('Bit Erorr Rate')
title('SNR-BER in LTE HARQ Implementation');
figure
semilogy(SNR, Retransmitted_Subframes);
grid
xlabel('SNR (dB)')
ylabel('Retransmitted Subframes')
title('Retransmitted Frames in LTE HARQ Implementation');


%%

%% 2.3 
% part 1 and 2
clear
clc
SNR = (5.5:0.5:10).';
Retransmitted_Subframes = zeros(size(SNR));
Errors = zeros(size(SNR));
num_subframes_per_frame = 1;
for index = 1: length(SNR)
    num_retransmitted_subframes = 0;
    numOfErrors = 0;
    modOrder = 16;
    bps = log2(modOrder); % Bits per symbol
    EbNo = 2.8; % Energy per bit to noise power spectral density ratio in dB
    EsNo = EbNo + 10*log10(bps); % Energy per symbol to noise power spectral density ratio in dB
    rng default

    turboEnc = comm.TurboEncoder('InterleaverIndicesSource','Input port');
    turboDec = comm.TurboDecoder('InterleaverIndicesSource','Input port','NumIterations',4);
    trellis = poly2trellis(4,[13 15 17],13);
    n = log2(turboEnc.TrellisStructure.numOutputSymbols);
    numTails = log2(turboEnc.TrellisStructure.numStates)*n;
    
    
    errRate = comm.ErrorRate;
    numFrames = 100;
    for pktIdx = 1:numFrames
        L = 1344; % Packet length in bits
        data = randi([0 1],L,1);
        
        
        M = L*(2*n - 1) + 2*numTails; % Output codeword packet length
        rate = L/M; % Coding rate for current packet
        %snrdB = EsNo + 10*log10(rate); % Signal to noise ratio in dB
        snrdB = SNR(index);
        noiseVar = 1./(10.^(snrdB/10)); % Noise variance 
        [f1, f2] = getf1f2(L);
        Idx = (0:L-1).';
        intrlvrIndices = mod(f1*Idx + f2*Idx.^2, L) + 1;
        %intrlvrIndices = randperm(L);
        encodedData = turboEnc(data,intrlvrIndices);
        data_with_CRC= CbCRCGenerator(encodedData, num_subframes_per_frame);
        modSignal = qammod(data_with_CRC,modOrder,'InputType','bit','UnitAveragePower',true);
        receivedSignal = awgn(modSignal,snrdB);
        demodSignal = qamdemod(receivedSignal,modOrder,'OutputType','llr', ...
            'UnitAveragePower',true,'NoiseVariance',noiseVar);
        %demodSignal = qamdemod(receivedSignal,modOrder,'OutputType','bit','UnitAveragePower',true);
        [y, err] = CbCRCDetector(demodSignal, num_subframes_per_frame);
        receivedBits = turboDec(-y,intrlvrIndices); % Demodulated signal is negated
        
        errorStats = errRate(data,receivedBits);
        num_retransmitted_subframes = num_retransmitted_subframes + sum(err);
        
        %fprintf('Bit error rate = %5.2e\nNumber of errors = %d\nTotal bits = %d\n',errorStats)

        
    end
    numOfErrors = numOfErrors + errorStats(2);
    Errors(index) = numOfErrors/134400;
    Retransmitted_Subframes(index) = num_retransmitted_subframes;
    
end
figure
semilogy(SNR, Errors);
grid
xlabel('SNR (dB)')
ylabel('Bit Erorr Rate')
title('SNR-BER in LTE HARQ Implementation');
figure
semilogy(SNR, Retransmitted_Subframes);
grid
xlabel('SNR (dB)')
ylabel('Retransmitted Subframes')
title('Retransmitted Frames in LTE HARQ Implementation');



%% Functions
function y = Channel_With_Burst_Error(x, burst_min_length)
start_point = randi([1 length(x)-burst_min_length]); 
end_point = start_point + burst_min_length;

while end_point < length(x)
    p=0.5;
    next_bit_correct =(rand(1)<p);
    if next_bit_correct == 1 
        end_point = end_point + 1;
    else
        break;
    end
end


y = x;
for i = start_point:end_point
    y(i) = 3;
end
    
 
end


function encoded_data = Encoder(x, coding_rate)

number_of_repeats = int32(1/coding_rate);
encoded_data = zeros(number_of_repeats*size(x,1), size(x,2));
index = 1;
while index <= length(encoded_data) - number_of_repeats + 1
    for i=1:length(x)
        for j=1:number_of_repeats
            encoded_data(index) = x(i);
            index = index + 1;
        end
    end
end

end

function decoded_data = Decoder(x, coding_rate)

number_of_repeats = int32(1/coding_rate);

decoded_data = zeros(int32(coding_rate*size(x,1)), size(x,2));
for i=1:length(decoded_data)
    if 1+i*number_of_repeats <= length(x)     
        decoded_data(i) = mode(x(1+(i-1)*number_of_repeats:1+i*number_of_repeats));
    end
    
end

end

function y = interleaver(x, ordered_array)

y = zeros(size(x));
for i = 1:length(ordered_array)
    y(i) = x(ordered_array(i));
    
end
end

function y = deinterleaver(x, ordered_array)
y = zeros(size(x));
for i=1:length(ordered_array)
    y(ordered_array(i)) = x(i);
end
end



function y = CbCRCGenerator(u, ChecksumsPerFrame)
%#codegen
crcgenerator  = comm.CRCGenerator('Polynomial','z^16 + z^12 + z^5 + 1','ChecksumsPerFrame', ChecksumsPerFrame);
y = crcgenerator(u);
end

function [msg, err] = CbCRCDetector(u,ChecksumsPerFrame)
%#codegen

crcdetector  = comm.CRCDetector('Polynomial','z^16 + z^12 + z^5 + 1','ChecksumsPerFrame', ChecksumsPerFrame);
[msg, err] = crcdetector(u);

end


function y=TurboEncoder(u, intrlvrIndices)
%#codegen
trellis = poly2trellis(4,[13 15 17],13);
turboenc = comm.TurboEncoder(trellis,intrlvrIndices);

y=turboenc(u);
end

function y=TurboDecoder(u, intrlvrIndices, maxIter)
%#codegen
trellis = poly2trellis(4,[13 15 17],13);
turbodec = comm.TurboDecoder(trellis,intrlvrIndices,maxIter);

y=turbodec(u);
end

function indices = IntrlvrIndices(blkLen)
%#codegen
[f1, f2] = getf1f2(blkLen);
Idx = (0:blkLen-1).';
indices = mod(f1*Idx + f2*Idx.^2, blkLen) + 1;
end

function y = QPSK_Modulator(u)
qpsk = comm.PSKModulator(4, 'BitInput', true, 'PhaseOffset', pi/4, 'SymbolMapping', 'Custom','CustomSymbolMapping', [0 2 3 1]);
y = qpsk(u);
end

function y = QPSK_Demodulator(u)
QPSK_Dem =  comm.PSKDemodulator('ModulationOrder', 4,'BitOutput', true,'PhaseOffset', pi/4, 'SymbolMapping', 'Custom','CustomSymbolMapping', [0 2 3 1]);
%QPSK_Dem = comm.PSKDemodulator('ModulationOrder', 4,'BitOutput', true,'PhaseOffset', pi/4, 'SymbolMapping', 'Custom','CustomSymbolMapping', [0 2 3 1],'DecisionMethod', 'Approximate log-likelihood ratio','VarianceSource', 'Input port');
y = QPSK_Dem(u);
end


function y = Scrambler(u, nS)
% Downlink scrambling
maxG=43200;
hSeqGen = comm.GoldSequence('FirstPolynomial',[1 zeros(1, 27) 1 0 0 1],...
'FirstInitialConditions', [zeros(1, 30) 1], ...
'SecondPolynomial', [1 zeros(1, 27) 1 1 1 1],...
'SecondInitialConditionsSource', 'Input port',...
'Shift', 1600,...
'VariableSizeOutput', true,...
'MaximumOutputSize', [maxG 1]);
hInt2Bit = comm.IntegerToBit('BitsPerInteger', 31);
% Parameters to compute initial condition
RNTI = 1;
NcellID = 0;
q =0;
% Initial conditions
c_init = RNTI*(2^14) + q*(2^13) + floor(nS/2)*(2^9) + NcellID;
% Convert initial condition to binary vector
iniStates = step(hInt2Bit, c_init);
% Generate the scrambling sequence
nSamp = size(u, 1);
seq = step(hSeqGen, iniStates, nSamp);
seq2=zeros(size(u));
seq2(:)=seq(1:numel(u),1);
% Scramble input with the scrambling sequence
y = xor(u, seq2);
end

function y = DescramblerSoft(u, nS)
% Downlink descrambling

maxG=43200;
hSeqGen = comm.GoldSequence('FirstPolynomial',[1 zeros(1, 27) 1 0 0 1],...
'FirstInitialConditions', [zeros(1, 30) 1], ...
'SecondPolynomial', [1 zeros(1, 27) 1 1 1 1],...
'SecondInitialConditionsSource', 'Input port',...
'Shift', 1600,...
'VariableSizeOutput', true,...
'MaximumOutputSize', [maxG 1]);
hInt2Bit = comm.IntegerToBit('BitsPerInteger', 31);

% Parameters to compute initial condition
RNTI = 1;
NcellID = 0;
q=0;
% Initial conditions
c_init = RNTI*(2^14) + q*(2^13) + floor(nS/2)*(2^9) + NcellID;
% Convert to binary vector
iniStates = step(hInt2Bit, c_init);
% Generate scrambling sequence
nSamp = size(u, 1);
seq = step(hSeqGen, iniStates, nSamp);
seq2=zeros(size(u));
seq2(:)=seq(1:numel(u),1);
% If descrambler inputs are log-likelihood ratios (LLRs) then
% Convert sequence to a bipolar format
seq2 = 1-2.*seq2;
% Descramble
y = u.*seq2;
end
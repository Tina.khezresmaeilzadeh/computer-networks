import json
import socket
from threading import Thread


def get_heading_and_body(data):
    data = ''.join((line + '\n') for line in data.decode().splitlines())
    request_head, request_body = data.split('\n\n', 1)
    request_head = request_head.splitlines()
    request_headline = request_head[1]
    return request_headline, request_body


class Tracker(Thread):

    def __init__(self, ip, port):
        Thread.__init__(self)
        self.ip = ip
        self.port = port
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.socket.bind((self.ip, self.port))
        self.Tracker_dict = {'failure reason': 'False', 'interval': 2, 'complete': 0, 'incomplete': 0, 'peers': []}

    def listen(self, connection, address):
        while True:
            try:
                data = connection.recv(1024)
                if data:
                    request_headline, request_body = get_heading_and_body(data)
                    Information_from_Peer = json.loads(request_body)
                    print('Received from tracker: ', str(Information_from_Peer))
                    if request_headline == 'start connection':
                        self.start_connection(connection=connection, address=address)
                else:
                    raise socket.error('The client disconnected')
            except:
                connection.close()
                return False

    def start_connection(self, connection, address):
        Response = "HTTP/1.1 200 OK\r\n\r\n%s" % str(json.dumps(self.Tracker_dict))
        connection.send(Response.encode())
        self.Tracker_dict['peers'].append({'ip': address[0], 'port': address[1]})

    def run(self):
        self.socket.listen()
        while True:
            connection, address = self.socket.accept()
            connection.settimeout(80)
            Thread(target=self.listen, args=(connection, address)).start()

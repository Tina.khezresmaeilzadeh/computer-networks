﻿## Final Project
### Bittorrent Implementation

 - Implementing a bittorrent network with several peers and trackers with multiple **.torrent** files.
 - Using socket programming for communication between peers and trackers.
 - Using HTTP requests and responses in order to implement the handshake between peers and trackers.
 - Usong bencode and sha256 hash for increasing security in conection between nodes.


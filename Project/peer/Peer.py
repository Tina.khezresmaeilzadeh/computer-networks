import ast
import json
import time
import socket
from threading import Thread
from tracker.Tracker import Tracker
import numpy as np

def handle_response(response):
    response = ''.join((line + '\n') for line in response.decode().splitlines())
    response_head, response_body = response.split('\n\n', 1)
    Information_from_Tracker = json.loads(response_body)
    print('received from server: ', repr(Information_from_Tracker))


class Peer(Thread):
    def __init__(self, meta_info_file_name):
        super(Peer, self).__init__()
        self.meta_info_file_name = meta_info_file_name
        self.ip = 0
        self.port = 0
        self.Peer_dict = {'info hash': 0, 'peer id': str(np.random.bytes(20)), 'port': 0, 'uploaded': 0, 'downloaded': 0,
                          'left': 'empty'}

    def send_request(self, request_type='start connection'):
        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
            s.connect((self.ip, self.port))
            request = "GET / HTTP/1.1\r\n" + str(request_type) + "\r\n\r\n" + str(json.dumps(self.Peer_dict))
            s.send(request.encode())
            data = s.recv(1024)
            handled_response = handle_response(data)

        return repr(data)

    def parse_meta_info_file(self):
        # Reading the MetaInfo File
        file = open(self.meta_info_file_name, "r")
        contents = file.read()
        meta_info_dict = ast.literal_eval(contents)
        file.close()
        # Finding port and ip of the tracker
        Tracker_URL = meta_info_dict['announce']
        Tracker_Information = Tracker_URL[7:].split(':')
        self.ip = str(Tracker_Information[0])
        self.port = int(Tracker_Information[1])
        self.Peer_dict['port'] = self.port

    def torrent_thread_pool(self):
        tracker1 = Tracker(ip=self.ip, port=self.port).start()

    def run(self):
        self.parse_meta_info_file()
        self.torrent_thread_pool()
        for i in range(5):
            time.sleep(2)
            self.send_request()
